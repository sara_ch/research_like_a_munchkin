import numpy as np

i = 1
j = 1
result = 0
fib = 0
while fib <= 4000000:
    fib = i + j
    i = j
    j = fib
    if fib % 2 == 0:
        result += fib

print("Here is the result:")
print(result)

